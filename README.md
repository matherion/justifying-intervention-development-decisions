# Justifying Intervention Development Decisions

This is the GitLab repository for the Justifying Intervention Development Decisions project by Marta Moreira Marques & Gjalt-Jorn Ygram Peters.

The rendered version of the main R Markdown file for this project is hosted by GitLab pages at https://matherion.gitlab.io/justifying-intervention-development-decisions.
